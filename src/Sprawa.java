
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Winiarek Michał
 */
public class Sprawa {
    
    private String signature; //tytuł sprawy
    private double charge; //Wartość dochodzonej należnośći
    private double payment; //Pobrana opłata za poszukiwanie majątku
    private List<Majatek> estates;
    
    public Sprawa(String signature, String charge, String payment, List<Majatek> estates){
        
        charge = charge.replace(",", ".");
        double c = Double.parseDouble(charge);
        this.charge = c;
        
        payment = payment.replace(",", ".");
        double p = Double.parseDouble(payment);
        this.payment = p;
        
        this.estates = new ArrayList();
        this.estates = estates; 
        this.signature = signature;
    }
    
    public String getSignature(){
        return signature;
    }
    
    public void setSignature(String signature){
        this.signature = signature;
    }
    
    public double getCharge(){
        return charge;
    }
        
    public void setCharge(double charge){
        this.charge = charge;
    }
    
    public double getPayment(){
        return payment;
    }   
    
    public void setPayment(String payment){
        double p = Double.parseDouble(payment);
        this.payment = p;
    }
        
    public List<Majatek> getEstates(){
        return estates;
    }
    
    public void setEstates(List<Majatek> estates){
        this.estates = estates;
    }
    
}
