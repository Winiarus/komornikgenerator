/**
 *
 * @author Winiarek Michał
 */
public class Majatek {
    
    private double value;
    private String name;
    private String date;
    private boolean pension;
    
    public Majatek(String value, String name, boolean pension, String date){  
        value = value.replace(",", ".");
        double v;
        if(pension){
            v = Double.parseDouble(value)*12.0;
        }else{
            v = Double.parseDouble(value);
        }
        this.value = v;
        this.name = name;
        this.pension = pension;
        this.date = date;
    }
    
    public boolean getPension(){
        return pension;
    }
    
    public void setPension(boolean pension){
        this.pension = pension;
    }
    
    public double getValue(){
        return value;
    }
    
    public void setValue(double value){
        this.value = value;
    }
    
    public String getDate(){
        return date;
    }
    
    public void setDate(String date){
        this.date = date;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
}
